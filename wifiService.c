#include "wifiService.h"

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_err.h"
#include "nvs_flash.h"

#include "esp_log.h"

#define TASK_DEPTH                          (5*1024)
#define TASK_PRIORITY                       (3)
#define TASK_DELAY_S                        (1)

#define WIFI_AP_MAX_CONNECTION              (5)
#define MAX_ATTEMPS_TO_CONNECT              (3)
#define MIN_RECONNECT_PERIOD                (10)

static const char* TAG = "[wifiService] ";

static TaskHandle_t wifiServise_task_handle = NULL;
static wifiService_config_t* wifiService_config = NULL;
static TimerHandle_t reconn_timer = NULL;

static wifi_event_t wifiService_status;
static uint8_t try_to_connect = MAX_ATTEMPS_TO_CONNECT;

static void wifi_init();
static void wifi_init_sta();
static void wifi_init_ap();
static void wifiService_task();

esp_err_t wifiService_init(wifiService_config_t config)
{
    if(wifiService_config)
    {
        ESP_LOGE(TAG, "%s Service already inited!", __func__);
        return ESP_FAIL;
    }

    wifiService_config = (wifiService_config_t*) malloc(sizeof(wifiService_config_t));

    if(config.reconnect_period_s < MIN_RECONNECT_PERIOD)
        wifiService_config->reconnect_period_s = MIN_RECONNECT_PERIOD;
    else wifiService_config->reconnect_period_s = config.reconnect_period_s;

    wifiService_config->ap_ssid = config.ap_ssid;
    wifiService_config->ap_password = config.ap_password;
    wifiService_config->ap_max_connection = config.ap_max_connection;

    return ESP_OK;
}

esp_err_t wifiService_start()
{
    esp_err_t err_code;
    wifi_config_t wifi_config = {0};

    if(wifiServise_task_handle)
    {
        ESP_LOGE(TAG, "%s Service already started!", __func__);
        return ESP_FAIL;
    }

    err_code = wifiService_Get_sta_settings(&wifi_config);
    if(err_code != ESP_OK)
    {
        if(err_code != ESP_ERR_NVS_NOT_FOUND)
            return err_code;
        else 
            ESP_LOGW(TAG, "%s Have no saved settings", __func__);
    }

    xTaskCreate(wifiService_task,
                "wifiService_task",
                TASK_DEPTH,
                NULL,
                TASK_PRIORITY,
                &wifiServise_task_handle);

    return ESP_OK;
}

esp_err_t wifiService_stop()
{
    if(!wifiServise_task_handle)
    {
        ESP_LOGE(TAG, "%s Service didn't started yet!", __func__);
        return ESP_FAIL;
    }

    vTaskDelete(wifiServise_task_handle);
    wifiServise_task_handle = NULL;

    return ESP_OK;
}

esp_err_t wifiService_denit()
{
    if(wifiServise_task_handle)
        wifiService_stop();

    if(!wifiService_config)
    {
        ESP_LOGE(TAG, "%s Service didn't inited yet!", __func__);
        return ESP_FAIL;
    }

    return ESP_OK;
}

static void reconnec_timer_callback( TimerHandle_t pxTimer )
{
    wifi_config_t wifi_config = {0};
    try_to_connect = MAX_ATTEMPS_TO_CONNECT; //! restore reconnect attemps

    //! If we have saved settings
    if(wifiService_Get_sta_settings(&wifi_config) == ESP_OK)
    {
        ESP_ERROR_CHECK(esp_wifi_stop());
        wifi_init_sta(wifi_config);
        ESP_ERROR_CHECK(esp_wifi_start());
        ESP_ERROR_CHECK(esp_wifi_connect());
    }
    xTimerDelete(reconn_timer, 0);
    reconn_timer = NULL;
}

static void wifiService_task()
{
    esp_err_t err_code; 
    uint8_t ap_connected_user = 0;
    wifi_config_t wifi_config = {0};
    bool have_settings = false;

    wifi_init();

    if(wifiService_Get_sta_settings(&wifi_config) == ESP_OK)
    {
        //! Have saved settings init STA
        wifi_init_sta(wifi_config);
        ESP_ERROR_CHECK(esp_wifi_connect());
    }
    else {
        //! Have no saved settings init AP
        strcpy((char*)wifi_config.ap.ssid, wifiService_config->ap_ssid);
        strcpy((char*)wifi_config.ap.password, wifiService_config->ap_password);

        wifi_init_ap(wifi_config);
    }

    ESP_ERROR_CHECK(esp_wifi_start());

    //! Main loop of wifiService task
    for(;;)
    {
        vTaskDelay(TASK_DELAY_S*1000 / portTICK_PERIOD_MS);

        //! Checking for settings
        if(wifiService_Get_sta_settings(&wifi_config) == ESP_OK)
            have_settings = true;
        else have_settings = false;

        switch(wifiService_status)
        {
            case WIFI_EVENT_AP_START:
                if(have_settings && reconn_timer == NULL)
                {
                    reconn_timer = xTimerCreate("TryRecconectTimer",
                                        (wifiService_config->reconnect_period_s
                                        *1000/portTICK_PERIOD_MS),
                                        pdFALSE,
                                        0,
                                        reconnec_timer_callback);
                    xTimerStart(reconn_timer, 0);
                }
                break;
            case WIFI_EVENT_STA_DISCONNECTED:
                if(try_to_connect == MAX_ATTEMPS_TO_CONNECT)
                    ESP_LOGI(TAG, "Try to connect to previos saved network");
                try_to_connect--;

                if(try_to_connect)
                {
                    err_code = esp_wifi_connect();
                    ESP_ERROR_CHECK(err_code);
                }else 
                {
                    ESP_LOGI(TAG, "Connection attempts failed, starts the AP mode");
                    //! Coping Wifi AP settings
                    strcpy((char*)wifi_config.ap.ssid,
                            wifiService_config->ap_ssid);
                    strcpy((char*)wifi_config.ap.password,
                            wifiService_config->ap_password);

                    //! Starting AP
                    err_code = esp_wifi_stop();
                    ESP_ERROR_CHECK(err_code);
                    wifi_init_ap(wifi_config);
                    err_code = esp_wifi_start();
                    ESP_ERROR_CHECK(err_code);
                }
                break;
            case WIFI_EVENT_STA_CONNECTED:
                //! STA connected, restore apptems
                try_to_connect = MAX_ATTEMPS_TO_CONNECT;
                break;
            case WIFI_EVENT_AP_STACONNECTED:
                ap_connected_user++;
                ESP_LOGI(TAG, "Users connected to AP: %d", ap_connected_user);
                if(reconn_timer != NULL) {
                    xTimerDelete(reconn_timer, 0);
                    reconn_timer = NULL;
                    if(ap_connected_user == 1)
                        ESP_LOGI(TAG, "Attempts to reconnect are stopped");
                }
                break;
            case WIFI_EVENT_AP_STADISCONNECTED:
                ap_connected_user--;
                ESP_LOGI(TAG,"User disconnected from the AP");
                if(have_settings && reconn_timer == NULL
                    && ap_connected_user == 0){
                    reconn_timer = xTimerCreate("TryReconnectTimer",
                                        (wifiService_config->reconnect_period_s
                                        *1000/portTICK_PERIOD_MS),
                                        pdFALSE,
                                        0,
                                        reconnec_timer_callback);
                    xTimerStart(reconn_timer, 0);
                    ESP_LOGI(TAG,"Noone connected to the AP");
                }
                break;
            default: 
                break;   
        }
        //! Changing status to receive a new event
        wifiService_status = WIFI_EVENT_MAX;
    }
    vTaskDelete(NULL);
}

static void event_handler(void* arg, esp_event_base_t event_base, 
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT)
    {
        wifiService_status = event_id; //! Updating status

        switch (event_id)
        {
            case WIFI_EVENT_STA_START:
                ESP_LOGD(TAG, "WIFI_EVENT_STA_START");
                break;
            case WIFI_EVENT_STA_STOP:
                ESP_LOGD(TAG, "WIFI_EVENT_STA_STOP");
                break;
            case WIFI_EVENT_STA_CONNECTED:
                ESP_LOGD(TAG, "WIFI_EVENT_STA_CONNECTED");
                break;
            case WIFI_EVENT_STA_DISCONNECTED:
                ESP_LOGD(TAG, "WIFI_EVENT_STA_DISCONNECTED");
                break;
            case WIFI_EVENT_AP_START:
                ESP_LOGD(TAG, "WIFI_EVENT_AP_START");
                break;
            case WIFI_EVENT_AP_STOP:
                ESP_LOGD(TAG, "WIFI_EVENT_AP_STOP");
                break;
            case WIFI_EVENT_AP_STACONNECTED:
                ESP_LOGD(TAG, "WIFI_EVENT_AP_STACONNECTED");
                break;
            case WIFI_EVENT_AP_STADISCONNECTED:
                ESP_LOGD(TAG, "WIFI_EVENT_AP_STADISCONNECTED");
                break;
            
            default:
                ESP_LOGD(TAG, "WIFI_EVENT %d", event_id);
                break;
        }
    }

    if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "Got ip: %s",
                 ip4addr_ntoa((ip4_addr_t*)&event->ip_info.ip));
    }
}

void wifi_init()
{
	esp_err_t err_code;

    tcpip_adapter_init();
    esp_netif_init();

    err_code = esp_event_loop_create_default();
    ESP_ERROR_CHECK(err_code);

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    cfg.nvs_enable = false;

    err_code = esp_wifi_init(&cfg);
    ESP_ERROR_CHECK(err_code);

    err_code = esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL);
    ESP_ERROR_CHECK(err_code);
    err_code = esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL);
    ESP_ERROR_CHECK(err_code);

    err_code = esp_wifi_set_mode(WIFI_MODE_NULL);
    ESP_ERROR_CHECK(err_code);

    err_code = esp_wifi_start();
    ESP_ERROR_CHECK(err_code);

    ESP_LOGD(TAG, "wifi_init finished!");

}

 void wifi_init_sta(wifi_config_t wifi_config)
{
    esp_err_t err_code;

    err_code = esp_wifi_set_mode(WIFI_MODE_STA);
    ESP_ERROR_CHECK(err_code);
    
    err_code = esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config);
    ESP_ERROR_CHECK(err_code);

    ESP_LOGI(TAG, "STA mode inited with SSID: %s, password: %s",
            (char*)wifi_config.sta.ssid, (char*)wifi_config.sta.password);
    ESP_LOGD(TAG, "wifi_init_sta finished.");
}

 void wifi_init_ap(wifi_config_t wifi_config)
{
    esp_err_t err_code;
    static bool warn_msg_showed = false;

    wifi_config.ap.ssid_len = strlen((char*)wifi_config.ap.ssid);
    wifi_config.ap.max_connection = WIFI_AP_MAX_CONNECTION;

    //! Default ESP AP ssid will be ESP32_MAC
    uint8_t eth_mac[6];
    err_code = esp_wifi_get_mac(ESP_IF_WIFI_AP, eth_mac);
    ESP_ERROR_CHECK(err_code);
    char ssid_with_mac[33];
    snprintf(ssid_with_mac, sizeof(ssid_with_mac), "ESP32_%02X%02X%02X",
                eth_mac[3], eth_mac[4], eth_mac[5]);

    if (strlen((char*)wifi_config.ap.ssid) < 2)
        strcpy((char*)wifi_config.ap.ssid, ssid_with_mac);

    if (strlen((char*)wifi_config.ap.password) < 8) {
        if(!warn_msg_showed)
        {
            ESP_LOGW(TAG, "AP password length less than 8!");
            ESP_LOGW(TAG, "Wifi authmode - WIFI_AUTH_OPEN");
            warn_msg_showed = true;
        }
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    } else
        wifi_config.ap.authmode = WIFI_AUTH_WPA_WPA2_PSK;

    err_code = esp_wifi_set_mode(WIFI_MODE_AP);
    ESP_ERROR_CHECK(err_code);

    err_code = esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config);
    ESP_ERROR_CHECK(err_code);

    ESP_LOGD(TAG, "wifi_init_ap finished.");
    ESP_LOGD(TAG, "AP SSID: %s, password: %s",
             (char*)wifi_config.ap.ssid, (char*)wifi_config.ap.password);
}
