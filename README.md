# ESP32 wifiService

### Including in your project
    git submodule add https://Grin00@bitbucket.org/Grin00/esp32_wifiservice.git components/wifiService

### Building
Component have both build system make and Cmake 

### Usage
**ATTENTION!** WifiService needs initialization NVS before usage! 
```C++
    //! initialization and starts wifiService
    wifiService_config_t wifiService_config = WIFI_SERVICE_DEF_CONFIG;
    wifiService_init(wifiService_config);
    wifiService_start();
    
    //! When you have received wifi configurations just use 
    //! and service will connect in reconnection period
    wifiService_Save_sta_settings(wifi_config)
```