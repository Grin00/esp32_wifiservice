#ifndef WIFISERVICE_H
#define WIFISERVICE_H

#include "esp_wifi.h"

#define WIFI_SERVICE_DEF_CONFIG     {.reconnect_period_s = 20,\
                                     .ap_ssid = "",\
                                     .ap_password = "",\
                                     .ap_max_connection = 3}

/** @brief Configuration data for wifiService. */
typedef struct {
    uint32_t reconnect_period_s;    /**< Try to reconnect period in seconds. */
    char* ap_ssid;                  /**< SSID of ESP32 soft-AP. */
    char* ap_password;              /**< Password of ESP32 soft-AP. */
    uint32_t ap_max_connection;     /**< Max number of stations allowed to connect in, default 4, max 4 */
} wifiService_config_t;

/**
 * @brief Initialization of wifiService
 * 
 * @param   config  configuration of wifiService 
 * @return
 *      - ESP_OK: succeed
 *      - ESP_FAIL: fail
 */
esp_err_t wifiService_init(wifiService_config_t config);

/**
 * @brief Deinitialization of wifiService
 * 
 * @param   none
 * @return
 *      - ESP_OK: succeed
 *      - ESP_FAIL: failed
 */
esp_err_t wifiService_denit();

/**
 * @brief Start of wifiService
 * 
 * @param   none
 * @return
 *      - ESP_OK: succeed
 *      - ESP_FAIL: failed
 */
esp_err_t wifiService_start();

/**
 * @brief Stop of wifiService
 * 
 * @param   none
 * @return
 *      - ESP_OK: succeed
 *      - ESP_ERR_NVS_NOT_INITIALIZED: NVS not initialized
 *      - ESP_FAIL: failed
 */
esp_err_t wifiService_stop();

/**
 * @brief Get station SSID and password from NVS
 * 
 * @param   wifi_config configuration of station mode (SSID & password)
 * @return
 *      - ESP_OK: succeed
 *      - ESP_ERR_NVS_NOT_FOUND: wifiService have no saved settings in NVS
 *      - ESP_ERR_NVS_NOT_INITIALIZED: NVS not initialized
 *      - others: refer to the erro code in esp_err.h
 */
esp_err_t wifiService_Get_sta_settings(wifi_config_t *wifi_config);

/**
 * @brief Save station SSID and password to NVS
 * 
 * @param wifi_config configuration of station mode (SSID & password)
 * @return
 *      - ESP_OK: succeed
 *      - ESP_ERR_INVALID_ARG: failed to save configuration. SSID or password too small
 *      - ESP_ERR_NVS_NOT_INITIALIZED: NVS not initialized
 *      - others: refer to the erro code in esp_err.h
 */
esp_err_t wifiService_Save_sta_settings(wifi_config_t wifi_config);

#endif//WIFISERVICE_H