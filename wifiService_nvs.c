#include "wifiService.h"

#include "string.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_err.h"

#include "esp_log.h"

#define MIN_STA_SSID_LEN                (1)
#define MIN_STA_PASSWORD_LEN            (8)

#define WIFI_NVS_NAMESPACE              "wifi_namespace"
#define WIFI_STA_SSID_KEY               "sta_ssid"
#define WIFI_STA_PASSWORD_KEY           "sta_password"

static const char* TAG = "[wifiService_nvs] ";

esp_err_t wifiService_Get_sta_settings(wifi_config_t *wifi_config)
{
    esp_err_t ret = ESP_OK;
    nvs_handle WifiManager_nvs_handle;

    ret = nvs_open(WIFI_NVS_NAMESPACE, NVS_READONLY, &WifiManager_nvs_handle);
    if (ret != ESP_OK)
    {
        if(ret != ESP_ERR_NVS_NOT_FOUND)
            ESP_LOGE(TAG, "%s %s ", __func__, esp_err_to_name(ret));

        return ret;
    }

    size_t wifi_ssid_size = sizeof(wifi_config->sta.ssid);
    ret = nvs_get_blob(WifiManager_nvs_handle, WIFI_STA_SSID_KEY, wifi_config->sta.ssid, &wifi_ssid_size);
    if (ret != ESP_OK) return ret;

    size_t wifi_password_size = sizeof(wifi_config->sta.password);
    ret = nvs_get_blob(WifiManager_nvs_handle, WIFI_STA_PASSWORD_KEY, wifi_config->sta.password, &wifi_password_size);
    if (ret != ESP_OK) return ret;

    nvs_close(WifiManager_nvs_handle);

    return ret;
}

esp_err_t wifiService_Save_sta_settings(wifi_config_t wifi_config)
{
    esp_err_t ret = ESP_OK;
    nvs_handle WifiManager_nvs_handle;

    ret = nvs_open(WIFI_NVS_NAMESPACE, NVS_READWRITE, &WifiManager_nvs_handle);
    if (ret != ESP_OK)
    {
        ESP_LOGI(TAG, "%s %s ", __func__, esp_err_to_name(ret));
        return ret;
    }

    if(strlen((char*)wifi_config.sta.ssid) <= MIN_STA_SSID_LEN)
    {
        ESP_LOGE(TAG, "%s station SSID too small ", __func__);
        return ESP_ERR_INVALID_ARG;
    }

    ret = nvs_set_blob(WifiManager_nvs_handle, WIFI_STA_SSID_KEY, wifi_config.sta.ssid, 32);
    ESP_ERROR_CHECK(ret);

    if(strlen((char*)wifi_config.sta.password) <= MIN_STA_PASSWORD_LEN)
    {
        ESP_LOGE(TAG, "%s station password too small ", __func__);
        return ESP_ERR_INVALID_ARG;
    }

    ret = nvs_set_blob(WifiManager_nvs_handle, WIFI_STA_PASSWORD_KEY, wifi_config.sta.password, 64);
    ESP_ERROR_CHECK(ret);

    ret = nvs_commit(WifiManager_nvs_handle);
    ESP_ERROR_CHECK(ret);

    nvs_close(WifiManager_nvs_handle);

    return ret;
}
